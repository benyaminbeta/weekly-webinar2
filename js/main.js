document.addEventListener('DOMContentLoaded', () => {

    // Create a new Date object with the desired date in reverse format
    var endDate = new Date(2023, 07, 06,17,00,00); // month is 0-based index
  
    // Unix timestamp (in seconds) to count down to
    var countdownTo = endDate.getTime() / 1000;
  
    // Set up FlipDown
    var flipdown = new FlipDown(countdownTo)
  
      // Start the countdown
      .start()
  
      // Do something when the countdown ends
      .ifEnded(() => {
        console.log('The countdown has ended!');
      });
  
    // Toggle theme
    var interval = setInterval(() => {
      let body = document.body;
    }, 5000);
  
    // Show version number
    var ver = document.getElementById('ver');
    ver.innerHTML = flipdown.version;
  
    // Display the end date in reverse format
    var endDateElem = document.getElementById('end-date');
    endDateElem.innerHTML = endDate.toLocaleDateString('en-GB');
  });
  